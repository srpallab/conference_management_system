# Generated by Django 2.2.7 on 2019-11-21 13:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrationtable',
            name='is_attend',
            field=models.CharField(choices=[('YES', 'YES'), ('NO', 'NO')], max_length=3),
        ),
        migrations.AlterField(
            model_name='registrationtable',
            name='is_paper',
            field=models.CharField(choices=[('YES', 'YES'), ('NO', 'NO')], max_length=3),
        ),
        migrations.AlterField(
            model_name='registrationtable',
            name='is_poster',
            field=models.CharField(choices=[('YES', 'YES'), ('NO', 'NO')], max_length=3),
        ),
    ]
