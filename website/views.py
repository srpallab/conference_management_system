from django.shortcuts import redirect
from django.views.generic import TemplateView, FormView
from .forms import RegistrationForm
from .models import Registration
from django.core.mail import send_mail


class IndexPage(TemplateView):
    """Documentation for IndexPage

    """
    template_name = 'website/index.html'


class InformationPage(TemplateView):
    """Documentation for InformationPage

    """
    template_name = 'website/info.html'


class AuthorPage(TemplateView):
    """Documentation for AuthorPage

    """
    template_name = 'website/author.html'


class CommitteePage(TemplateView):
    """Documentation for Committee

    """
    template_name = 'website/committee.html'


class ThankYouPage(TemplateView):
    """Documentation for ThankYouPage

    """
    template_name = 'website/thankyou.html'


class RegistrationPage(FormView):
    """Documentation for Registration

    """
    # print(RegistrationForm)
    template_name = 'website/register.html'
    form_class = RegistrationForm
    success_url = 'website/thankyou.html'

    def form_valid(self, form):
        form.save()
        send_mail(
            'Thank You For Registration CCFS 2020 BD.',
            '''Congratulation! You have succefully registered. Looking forward to your payment details. Please reply payment details to this email address.''',
            'ccfs2020bd@gmail.com',
            [form.cleaned_data['email']]
        )
        return redirect('thankyou')
