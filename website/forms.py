from django.utils.translation import gettext_lazy as _
from django.forms import ModelForm, TextInput
from .models import Registration


class RegistrationForm(ModelForm):
    class Meta:
        model = Registration
        exclude = ['created_on']
        labels = {
            'first_name': _('SurName'),
            'last_name': _('Given Name'),
            'affiliation': _('Affiliation'),
            'address': _('Maling Address'),
            'city': _('City'),
            'country': _('Country'),
            'phone': _('Phone'),
            'fax': _('Fax'),
            'ws_phone': _('Viber or Whatsup Number'),
            'email': _('E-mail'),
            'plus_one_persons': _('Name And Relation of Accompanying Person'),
            'title_of_presntration': _('Tentative Title of Presentration:'),
            'is_attend': _('I am interested to attend the seminar'),
            'is_paper': _('I am interested to present a paper and enclosed herewith an abstract'),
            'is_poster': _('I am interested to participate in poster session'),
        }
        widgets = {
            'first_name': TextInput(attrs={'class': ''}),
            'affiliation': TextInput(attrs={'class': 'input-field col s12'})
        }
