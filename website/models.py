from django.db import models


class Registration(models.Model):
    """Documentation for RegistrationTable

    """
    SOME_CHOICES = (('YES', 'YES'), ('NO', 'NO'))
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    affiliation = models.CharField(max_length=60)
    address = models.CharField(max_length=155)
    city = models.CharField(max_length=10)
    country = models.CharField(max_length=10)
    phone = models.CharField(max_length=11)
    fax = models.CharField(max_length=11)
    ws_phone = models.CharField('Viber or Whatsup Number', max_length=11)
    plus_one_persons = models.CharField(max_length=120)
    email = models.EmailField()
    title_of_presntration = models.CharField(max_length=225)
    is_attend = models.CharField(max_length=3, choices=SOME_CHOICES)
    is_paper = models.CharField(max_length=3, choices=SOME_CHOICES)
    is_poster = models.CharField(max_length=3, choices=SOME_CHOICES)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.first_name + ' ' + self.last_name
