from django.urls import path
from .views import IndexPage, InformationPage, CommitteePage, RegistrationPage, ThankYouPage, AuthorPage


urlpatterns = [
    path('', IndexPage.as_view(template_name='website/index.html'), name='index'),
    path('info/', InformationPage.as_view(), name='info'),
    path('author', AuthorPage.as_view(), name='author'),
    path('committee/', CommitteePage.as_view(), name='committee'),
    path('register/', RegistrationPage.as_view(), name='register'),
    path('thankyou/', ThankYouPage.as_view(), name='thankyou'),

]
