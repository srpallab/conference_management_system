from django.contrib import admin
from .models import Registration

admin.site.site_header = "DashBoard"

# admin.site.register(Registration)
@admin.register(Registration)
class CustomRegistrationAdmin(admin.ModelAdmin):
    """Documentation for CustomRegistrationAdmin

    """
    list_display = ('__str__', 'email', 'ws_phone',
                    'city', 'country', 'is_attend', 'is_paper', 'is_poster')
